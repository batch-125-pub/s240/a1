//1. Adding users:
db.users.insertMany([
    {
        "firstName" : "Diane",
	    "lastName" : "Murphy",
	    "email" : "dmurphy@mail.com",
	    "isAdmin" : false,
	    "isActive" : true
    },
    {
        "firstName" : "Mary",
	    "lastName" : "Patterson",
	    "email" : "mpatterson@mail.com",
	    "isAdmin" : false,
	    "isActive" : true
    },
    {
    	"firstName" : "Jeff",
	    "lastName" : "Firrelli",
	    "email" : "jfirrelli@mail.com",
	    "isAdmin" : false,
	    "isActive" : true
	},
	{
		"firstName" : "Gerard",
	    "lastName" : "Bondur",
	    "email" : "gbondur@mail.com",
	    "isAdmin" : false,
	    "isActive" : true
	},
	{
		"firstName" : "Pamela",
	    "lastName" : "Castillo",
	    "email" : "pcastillo@mail.com",
	    "isAdmin" : true,
	    "isActive" : false
	},
	{
		"firstName" : "George",
	    "lastName" : "Vanauf",
	    "email" : "gvanauf@mail.com",
	    "isAdmin" : true,
	    "isActive" : true
	} 
])


//2. Adding courses:
db.courses.insertMany([
    {
        "Name":"Professional Development",
        "Price": 10000
    },
    {
        "Name": "Business Processing",
        "Price": 13000
    }
    
])

//3.
db.courses.updateMany(
	{
		"Name": "Professional Development"
	},
	{
		$set: {"Enrollees" : [ObjectId("6125c1e6734b166133a5a09c"),
			ObjectId("6125c284734b166133a5a09e")]}
	}

)
//updated twice (the other one is for Business Processing)
db.courses.updateMany(
	{
		"Name": "Business Processing"
	},
	{
		$set: {"Enrollees" : [ObjectId("6125c284734b166133a5a09f"),
			ObjectId("6125c1e6734b166133a5a09d")]}
	}

)


//4.
db.users.find(
	{
		"isAdmin":false
	}
)